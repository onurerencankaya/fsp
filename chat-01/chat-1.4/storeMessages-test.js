function storeMessages(messages) {
  console.log("--- storeMessages() is running ---")

  if (message) {
    console.log("Start state - messages data has been passed as a parameter.")
    try {
      if (document.body.contains(messages)) {
        console.log("End state - messages data has successfully been stored!")
      }
      console.log("End state - messages data could not be stored!")
    } catch (e) {
      console.log(e)
    }
  } else {
    console.log("Start state - no message data passed!")
  }
}

storeMessages('Talk to you soon!')