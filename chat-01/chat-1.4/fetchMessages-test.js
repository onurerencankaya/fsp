function fetchMessages(callback) {
  console.log("--- fetchMessages() is running ---")

  try {
    console.log("Start state - Is the DOM ready? DOM is ready!")
  } catch(e) {
    console.log("Start state - Is the DOM ready? unable to get document readyState")
    console.log(e)
    console.log("End state - message data from server: undefined")
  }
  console.log("End state - message data from server: message")

  if (!callback) {
    console.log("End state, part 2 - executing callback: no 'callback' function passed to 'fetchMessages' when it was run")
  } else {
    console.log("End state, part 2 - executing callback: callback function is running...")
  }
}

fetchMessages(() => {
  console.log('callback is running successfully')
})