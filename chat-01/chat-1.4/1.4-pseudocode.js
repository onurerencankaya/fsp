// 1.
// UX Step 1 - user opens chat app
// - app detects page load
// - app fetches all the messages from messages.json file
// - app saves all the messages to the DOM

// 2.
// (a)
// storeMessages() function gets passed messages parameter when it is called from fetchMessages
// storeMessages() stores messages in JSON format in the DOM
// - $.data()
// (b)
// make a GET request from the mock API and assign the response to messages variable in JSON format
// - $.ajax()
// call storeMessages() function, passing the messages variable
