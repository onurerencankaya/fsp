document.onreadystatechange = function () {
  if (document.readyState === 'complete') {
    console.log('DOM ready')
    fetchMessages(storeMessages)
  }
}

$(window).on('load', function() {
  $(".messages-container").animate({ scrollTop: $(document).height() }, 0)
})

let messageText = "test message"

// message parameter gets passed to sendMessage() function
function sendMessage(message) {
  // a div element gets created
  // - document.createElement()
  let node = document.createElement('div')
  // a text node gets created with the message parameter that is passed to the function
  // - document.createTextNode()
  const textnode = document.createTextNode(message)
  // the text node gets appended to the div node as a child
  // - Node.appendChild()
  node.appendChild(textnode)
  // message class gets added to class list of the div
  // sent class gets added to class list of the div
  // - Node.classList.add()
  node.classList.add('message')
  node.classList.add('sent')
  // the div with the text gets appended to the messages div as a child
  // - document.getElementById()
  // - document.querySelector()
  // - Node.appendChild()
  document.querySelector('.messages-container').appendChild(node)
  // scroll to the bottom of the messages to display the new message
  $(".messages-container").animate({ scrollTop: $(document).height() }, 500)
}

function storeMessages(messages) {
  // storeMessages() function gets passed messages parameter when it is called from fetchMessages
  if (messages) {
    console.log("Start state - messages data has been passed as a parameter.")
    // storeMessages() stores messages in JSON format in the DOM
    // - $.data()
    console.log('---from inside storeMessages---', messages)
    $("body").data("messages", messages)
    renderMessages()
  } else {
    console.log("Start state - no message data passed!")
  }
}

function fetchMessages(storeMessages) {
  console.log('---fetching messages---')
  // make a GET request from the mock API and assign the response to messages variable in JSON format
  // - $.ajax()
  $.ajax({
    url: "data/messages.json",
    type: 'GET',
    success: function(response) {
      // call storeMessages() function, passing the messages variable
      console.log('---ajax response---', response)
      storeMessages(response)
    },
    error: function(xhr) {
      console.log(`An error occured: ${xhr.status} ${xhr.statusText}`)
      alert(`An error occured: ${xhr.status} ${xhr.statusText}`)
    }
  })
}

function renderMessages() {
  console.log('---rendering messages---')
  let messages = $("body").data("messages")
  // we map the messages array
  messages.map((message) => {
    // for every message object a div element gets created
    let node = document.createElement('div')
    // a text node gets created with the message body
    const textNode = document.createTextNode(message.body)
    // we append the message body to the div as a child
    node.appendChild(textNode)
    // we add the 'message' class to all the divs
    node.classList.add('message')
    if (message.user === "user01") {
      // we add 'sent' class if the user is user01
      node.classList.add('sent')
    } else if (message.user === "user02") {
      // we add 'received' class if the user is user02
      node.classList.add('received')
    }
    // we append all the messages to the messages-container
    document.querySelector('.messages-container').appendChild(node)
    $(".messages-container").animate({ scrollTop: $(document).height() }, 0)
  })
  console.log('---from inside renderMessages---', messages)
}