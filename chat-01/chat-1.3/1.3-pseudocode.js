// 1. 
// UX Step 1 - user opens chat app
  // app detects page load
  // app console.logs 'DOM ready'

// UX Step 2 - page display all the messages
  // app shows all the messages

// UX Step 3 - user types in the input field and clicks send
  // user types in the input field
  // user clicks send or hits enter
  // app detects onclick event
  // app displays the new message on the screen
  // app clears the input field

// 2.
  // a) The DOM should have one more message appended

  // b) The input that is typed in the input field needs to be passed to the sendMessage function

  // c) 
  // message parameter gets passed to sendMessage() function
  // a div element gets created
  // - document.createElement()
  // a text node gets created with the message parameter that is passed to the function
  // - document.createTextNode()
  // the text node gets appended to the div node as a child
  // - Node.appendChild()
  // message class gets added to class list of the div
  // sent class gets added to class list of the div
  // - Node.classList.add()
  // the div with the text gets appended to the messages div as a child
  // - document.getElementById()
  // - document.querySelector()
  // - Node.appendChild()
