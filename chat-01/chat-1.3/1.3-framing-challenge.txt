1. As a chat app user, I want to be able to send a message to a friend so that we can have conversations online.

2.
  1 - user opens the app
  2 - app scrolls to the bottom of the messages view and displays all the previously sent and received messages
  3 - user types in the input field and clicks send button or hits enter
  4 - app displays the sent message in a chat bubble on the right side of the screen and clears the input field
