// 1.
// UX Step 1 - user opens chat app
// - app detects page load
// - app fetches all the messages from messages.json file
// - app saves all the messages to the DOM

// UX Step 2 - page displays
// - app gets messages from the DOM
// - app maps all the fetched messages to the screen

// 2.
  // (a) What format are your messages in when they're stored in the client, using jQuery data? (start)
  // They are an array of objects

  // (b) What format do you need them to be in to add them to the DOM? (end)
  // They need to be look like <div>message</div>

  // (c) What are possible steps for getting from the 'start' format to the 'end' format? (List out any steps you can think of, then keep going.  It's okay if you don't know all of them, at this stage)
  // we get the array of objects we stored from the DOM
  // we map the array
  // we get the message body of every item
  // we map them to look like <div>message.body</div>

    // (a) If you had a message string how would you get it ready to add to the DOM?
      // a div element gets created
      // - document.createElement()
      // a text node gets created with the string
      // - document.createTextNode()
      // the text node gets appended to the div node as a child
      // - Node.appendChild()

    // (b) If you had data for a single message in object format, how would you access the message text?
    // message.body

    // (c) If you had an array of objects, what are some coding techniques for executing code on each individual item in the array?
    // - Array.map()
    // - Array.forEach()

    // (d) How might you use some combination of techniques above to add 'chat bubbles' to the DOM for each message, one at a time?
    // - Array.map()
    // - Array.push()
    // - Array.concat()

    // (e) BONUS: Alternatively, how might you use some combination of techniques above to add 'chat bubbles' to the DOM for each message, all at once?
    // >> HINT: reflect on JS Arrays map() and join() exercises


