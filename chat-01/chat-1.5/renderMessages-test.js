function renderMessages() {
  console.log("--- renderMessages() is running ---")

  // Testing "start" state
  let messagesJson = [{id: "msg01", user: "user01", body: "Hi!"},{id: "msg02", user: "user01", body: "How's it going?"}]
  console.log("Start state - JSON data from storage:", messagesJson)

  // (pseudocode for getting from "start" to "end" will go here)

  // Testing "end" state
  let messageHTML = '<div>Hi!</div>'
  console.log("End state - message ready to add to DOM:", messageHTML)
}
